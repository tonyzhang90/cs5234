package algorithm;

import hashfunctions.myHashFunction;

import java.util.ArrayList;
import java.util.Collections;

public class Algo2 extends Algo {
    final int A;
    final COUNTER counter;
    final myHashFunction[] functions;

    public Algo2(int a, COUNTER counter, myHashFunction[] functions) {
        this.A = a;
        this.counter = counter;
        this.functions = functions;
    }

    public int getMedian(ArrayList<Integer> queryList) {
        Collections.sort(queryList);
        int middle = queryList.size() / 2;
        middle = middle % 2 == 0 ? middle - 1 : middle;
        return queryList.get(middle);
    }

    @Override
    public int query(int x) {
        ArrayList<Integer> queryList = new ArrayList<>();
        for (int i = 0; i < A; i++) {
            int j=functions[i].hash(x);
            int neighbor;
            if(j%2==0){//even
                neighbor=counter.value(i,j+1);
            }else {//odd
                neighbor=counter.value(i,j-1);
            }

            int estimate=counter.value(i,j )- neighbor;

            queryList.add(estimate);
        }
        return getMedian(queryList);
    }
}
