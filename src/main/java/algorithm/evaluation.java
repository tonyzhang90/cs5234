package algorithm;

import data.Streams;

import java.util.Arrays;
import java.util.stream.IntStream;

public class evaluation {


    public static double RMSE(Algo algo, Streams data, int N){
        double rmse = 0;
//        double prmse=0;
//        int [] results=new int[N];
//        IntStream.range(0,N).parallel().forEach(i->{
//            Integer x = data.get(i);
//            int offset = algo.query(x) - data.query(x);
//            results[i]= offset * offset;
//        });
//
//        for(int i=0;i<N;i++){
//            prmse+=results[i];
//        }

       // int sum = Arrays.stream(results).parallel().sum();//wrong results not sure why..

        for (int i = 0; i < N; i++) {
            Integer x = data.get(i);
            int offset = algo.query(x) - data.query(x);
            rmse += offset * offset;
        }
        return Math.sqrt(rmse / N);
    }

    public static double MSE(Algo algo, Streams data, int N){
        double mse = 0;
        for (int i = 0; i < N; i++) {
            Integer x = data.get(i);
            int offset = Math.abs(algo.query(x) - data.query(x));
            mse += offset;
        }
        return  mse / N;
    }
}
