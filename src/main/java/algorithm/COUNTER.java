package algorithm;

import java.util.HashMap;

public class COUNTER {
    HashMap<Integer, Integer>[] counter;

    public COUNTER(int A,int B) {
        this.counter = new HashMap[A];
        for(int i=0;i<A;i++){
            counter[i]=new HashMap<>();
            for (int b = 0; b < B; b++) {
                counter[i].putIfAbsent(b, 0);
            }
        }
    }

    int value(int i, int j) {
        return counter[i].get(j);
    }

    public void increment(int i, int j) {
        Integer curr = value(i, j);
        counter[i].put(j, curr + 1);
    }
}
