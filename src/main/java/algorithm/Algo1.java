package algorithm;

import hashfunctions.myHashFunction;

import java.util.ArrayList;
import java.util.Collections;

public class Algo1 extends Algo {
    final int A;
    final COUNTER counter;
    final myHashFunction[] functions;

    public Algo1(int a, COUNTER counter, myHashFunction[] functions) {
        this.A = a;
        this.counter = counter;
        this.functions = functions;
    }

    public int getMedian(ArrayList<Integer> queryList) {
        Collections.sort(queryList);
        int middle = queryList.size() / 2;
        middle = middle % 2 == 0 ? middle - 1 : middle;
        return queryList.get(middle);
    }

    @Override
    public int query(int x) {
        ArrayList<Integer> queryList = new ArrayList<>();
        for (int i = 0; i < A; i++) {
            queryList.add(counter.value(i, functions[i].hash(x)));
        }
        return getMedian(queryList);
    }
}
