package hashfunctions;

import java.util.Random;
import java.util.stream.IntStream;

public class primeFunction extends myHashFunction {
    final int p;
    final int i;
    final int a;
    final int b;

    static boolean isPrime(int value) {
        if (value < 2) {
            return false;
        }
        if (value == 2 || value == 3) {
            return true;
        }
        for (int i = 2; i <= Math.sqrt(value); ++i) {
            if (value % i == 0) {
                return false;
            }
        }
        return true;
    }

    static int getPrime(int B) {
        /**
         * Bertrand's postulate (actually a theorem) states that if n > 3 is an integer, then there always exists at least one prime number p with n < p < 2n − 2.
         * A weaker but more elegant formulation is: for every n > 1 there is always at least one prime p such that n < p < 2n.
         */
        int temp = B + 1;
        while (true) {
            if (isPrime(temp)) {
                return temp;
            }
            temp++;
        }
    }

    public primeFunction(int i, int a, int b, int B, int prime) {
        super(B);
        this.i = i;//function i..
        this.a = a;
        this.b = b;
        this.p = prime;
    }

    public static primeFunction[] generateprimeFunction(int A, int B) {
        int prime = getPrime(B);
        primeFunction[] functions = new primeFunction[A];
        IntStream.range(0,A).parallel().forEach(i->{
            Random r = new Random();
            int a = 1 + r.nextInt(prime - 2);
            int b = r.nextInt(prime - 1);
            functions[i] = new primeFunction(i, a, b, B, prime);
        });
        return functions;
    }

    @Override
    public int hash(int x) {
        return ((a * x + b) % p) % B;
    }
}
