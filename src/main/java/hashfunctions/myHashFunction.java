package hashfunctions;

public abstract class myHashFunction {
    final int B;

    public myHashFunction(int b) {
        B = b;
    }

    public abstract int hash(int x);
}
