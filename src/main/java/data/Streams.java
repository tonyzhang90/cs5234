package data;

import algorithm.MapUtil;

import java.util.*;

public abstract class Streams extends ArrayList<Integer> {
    private final int n;
    HashMap<Integer, Integer> real_counter = new HashMap<>();


    public Streams(int n, int m) {

        this.n = n;
    }

    /**
     * return the #number of hottest elements
     *
     * @param number
     * @return
     */
    public int[] Hot(int number) {
        real_counter = MapUtil.descending(real_counter);
        int[] hot_word = new int[number];
        int i = 0;
        for (int key : real_counter.keySet()) {
            hot_word[i++] = key;
            if(i==10)break;
        }
        return hot_word;
    }

    /**
     * return the #number of coldest elements
     *
     * @param number
     * @return
     */
    public int[] Cold(int number) {
        real_counter = MapUtil.ascending(real_counter);
        int[] cold_word = new int[number];
        int i = 0;
        for (int key : real_counter.keySet()) {
            cold_word[i++] = key;
            if(i==10)break;
        }
        return cold_word;
    }

    public int query(int x) {
        return real_counter.get(x);
    }
}
