package data;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.HashMap;

/**
 * use indexing to help represent word.
 */
public class RealStreams extends Streams {

    private static String FILENAME = "../t8.shakespeare.txt";
    HashMap<String, Integer> dict = new HashMap<>();
    int index = 0;//it can support up to 2,147,483,647 different words.
    String delimiters = "\\s+|\\t|,|;|\\.|\\?|!|-|:|@|\\[|\\]|\\(|\\)|\\{|\\}|_|\\*|/";


    private File getFileFromURL() {
        URL url = this.getClass().getClassLoader().getResource(FILENAME);
        File file = null;
        try {
            file = new File(url.toURI());
        } catch (URISyntaxException e) {
            file = new File(url.getPath());
        } finally {
            return file;
        }
    }

    public RealStreams(int n, int m, String fileName) {
        super(n, m);
        FILENAME = fileName;
        try (BufferedReader br = new BufferedReader(new FileReader(FILENAME))) {
            String sCurrentLine;
            while ((sCurrentLine = br.readLine()) != null) {
                String[] words = sCurrentLine.split(delimiters);
                for (String word : words) {
                    if (!dict.containsKey(word)) {
                        dict.put(word, index++);
                    }
                    int item = dict.get(word);
                    real_counter.putIfAbsent(item, 0);
                    real_counter.put(item, real_counter.get(item) + 1);
                    this.add(item);
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
