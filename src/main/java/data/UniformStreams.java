package data;

import java.util.Random;

public class UniformStreams extends Streams {

    Random r = new Random();

    public UniformStreams(int n, int m) {
        super(n, m);
        for (int i = 0; i < n; i++) {
            int item = r.nextInt(m);
            real_counter.putIfAbsent(item, 0);
            real_counter.put(item, real_counter.get(item) + 1);
            this.add(item);
        }
    }
}
