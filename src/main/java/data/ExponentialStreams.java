package data;

import java.util.HashMap;

public class ExponentialStreams extends Streams {


    class DistributedRandomNumberGenerator {

        private HashMap<Integer, Double> distribution;
        private double distSum;

        public DistributedRandomNumberGenerator() {
            distribution = new HashMap<>();
        }

        public void addNumber(int value, double distribution) {
            if (this.distribution.get(value) != null) {
                distSum -= this.distribution.get(value);
            }
            this.distribution.put(value, distribution);
            distSum += distribution;
        }

        public int getDistributedRandomNumber() {
            double rand = Math.random();
            double ratio = 1.0f / distSum;
            double tempDist = 0;
            for (Integer i : distribution.keySet()) {
                tempDist += distribution.get(i);
                if (rand / ratio <= tempDist) {
                    return i;
                }
            }
            return 0;
        }

    }

    public ExponentialStreams(int n, int m) {
        super(n, m);

        DistributedRandomNumberGenerator drng = new DistributedRandomNumberGenerator();
        double p = 0.5;
        for (int i = 0; i < m; i++) {
            drng.addNumber(i, p);
            p = p * p;
        }

        for (int i = 0; i < n; i++) {
            int item = drng.getDistributedRandomNumber();
            real_counter.putIfAbsent(item, 0);
            real_counter.put(item, real_counter.get(item) + 1);
            this.add(item);
        }


    }
}
