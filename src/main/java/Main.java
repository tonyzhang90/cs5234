import algorithm.*;
import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import data.ExponentialStreams;
import data.RealStreams;
import data.Streams;
import data.UniformStreams;
import hashfunctions.myHashFunction;
import hashfunctions.primeFunction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.stream.IntStream;

public class Main {
    private static final Logger LOG = LoggerFactory.getLogger(Main.class);
    @Parameter(names = {"--A"}, description = "number of hash functions..")
    private static int A = 200;
    @Parameter(names = {"--B"}, description = "number of hash buckets..")
    private static int B = 200;

    @Parameter(names = {"--R"}, description = "Ratio of A and B")
    private static double R = -1.0;

    @Parameter(names = {"--N"}, description = "number of elements..")
    private static int N = 1000000;
    @Parameter(names = {"--M"}, description = "upper bound of value in elements..")
    private static int M = 500;

    @Parameter(names = {"-type"}, description = "type of data streams")
    private static int type = 2;

    @Parameter(names = {"-fileName"}, description = "Path to the realStream data")
    private static String fileName = "t8.shakespeare.txt";

    private static final int type_uniform = 0;
    private static final int type_expo = 1;
    private static final int type_real = 2;
    private static algorithm.COUNTER COUNTER;

    private static void initilize_counter() {
        COUNTER = new COUNTER(A, B);
    }

    public static void main(String[] args) {

        Main runner = new Main();
        JCommander cmd = new JCommander(runner);

        try {
            cmd.parse(args);
        } catch (ParameterException ex) {
            System.err.println("Argument error: " + ex.getMessage());
            cmd.usage();
            System.exit(1);
        }

        LOG.info("Program Started.");
        long start = System.currentTimeMillis();
        myHashFunction[] functions = primeFunction.generateprimeFunction(A, B);
        initilize_counter();
        Streams data;
        switch (type) {
            case type_uniform: {
                data = new UniformStreams(N, M);
                break;
            }
            case type_expo: {
                data = new ExponentialStreams(N, M);
                break;
            }
            case type_real: {
                data = new RealStreams(N, M, fileName);
                N = data.size();
                break;
            }
            default:
                data = new UniformStreams(N, M);
                break;
        }
        long end = System.currentTimeMillis();
        LOG.info("Data prepare takes:" + (end - start) + " ms");

        for (int x : data) {
            IntStream.range(0, A).parallel().forEach(a -> {
                int hash = functions[a].hash(x);
                COUNTER.increment(a, hash);
            });
        }
//
//        for (int x : data) {
//            for(int a=0;a<A;a++){
//                int hash = functions[a].hash(x);
//                COUNTER.increment(a, hash);
//            }
//        }
        long end2 = System.currentTimeMillis();
        LOG.info("Counter update takes:" + (end2 - end) + " ms");

        LOG.info("A:" + A + "\tB:" + B + "\tN:" + N + "\tM:" + M);
        double rmse;
        Algo algo1 = new Algo1(A, COUNTER, functions);
        Algo algo2 = new Algo2(A, COUNTER, functions);
//        //Algorithm A..
//        rmse = evaluation.RMSE(algo1, data, N);
//        double mse = evaluation.MSE(algo1, data, N);
//        System.out.println("RMSE of algorithm A:" + rmse + "\t MSE of algorithm A:" + mse);
//
//        //Algorithm B..
//
//
//        rmse = evaluation.RMSE(algo2, data, N);
//        mse = evaluation.MSE(algo2, data, N);
//        System.out.println("RMSE of algorithm B:" + rmse + "\t MSE of algorithm B:" + mse);


        int[] hot = data.Hot(10);
        rmse=0;
        for (int x : hot) {
            int offset =algo1.query(x) - data.query(x);
            rmse += offset * offset;
        }
        rmse = Math.sqrt(rmse / 10);
        System.out.println("HOT data evaluation...RMSE of algorithm A:" + rmse + "\t MSE of algorithm A:" + rmse);


        rmse=0;
        for (int x : hot) {
            int offset =algo2.query(x) - data.query(x);
            rmse += offset * offset;
        }
        rmse = Math.sqrt(rmse / 10);
        System.out.println("HOT data evaluation...RMSE of algorithm B:" + rmse + "\t MSE of algorithm A:" + rmse);


        int[] cold = data.Cold(10);
        rmse=0;
        for (int x : cold) {
            int offset =algo1.query(x) - data.query(x);
            rmse += offset * offset;
        }
        rmse = Math.sqrt(rmse / 10);
        System.out.println("Cold data evaluation...RMSE of algorithm A:" + rmse + "\t MSE of algorithm A:" + rmse);


        rmse=0;
        for (int x : cold) {
            int offset =algo2.query(x) - data.query(x);
            rmse += offset * offset;
        }
        rmse = Math.sqrt(rmse / 10);
        System.out.println("Cold data evaluation...RMSE of algorithm B:" + rmse + "\t MSE of algorithm B:" + rmse);


    }
}
