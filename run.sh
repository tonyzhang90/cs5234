#!/bin/bash


timestamp=$(date +%Y%m%d-%H%M)

outputPath=test-$timestamp

mkdir -p $outputPath

cd $outputPath

echo Benchmark initiated at $(date +%Y%m%d-%H%M)

# echo "Real distribution experiments"

# echo "Varying A"
# for A in 100 200 500 
# do
   # numactl --cpunodebind=0 --membind=0 java -Xms1024g -Xmx1024g -XX:+UseG1GC  -jar ../target/PS5-1.0-SNAPSHOT-jar-with-dependencies.jar -type 2 --A $A -fileName ../t8.shakespeare.txt >> Real_TestA.txt
# done                         

# echo "Varying B"
# for B in 100 200 500 
# do
   # numactl --cpunodebind=0 --membind=0 java -Xms1024g -Xmx1024g -XX:+UseG1GC  -jar ../target/PS5-1.0-SNAPSHOT-jar-with-dependencies.jar -type 2 --B $B -fileName ../t8.shakespeare.txt >> Real_TestB.txt
# done 


# echo "Varying both"
# for A in 100 200 500 
# do
    # for B in 100 200 500 
    # do
       # numactl --cpunodebind=0 --membind=0 java -Xms1024g -Xmx1024g -XX:+UseG1GC  -jar ../target/PS5-1.0-SNAPSHOT-jar-with-dependencies.jar -type 2 --A $A --B $B -fileName ../t8.shakespeare.txt >> Real_TestBoth.txt
    # done 
# done

# echo "Uniform distribution experiments"
# echo "Varying A"
# for A in 100 200 500 
# do
   # numactl --cpunodebind=0 --membind=0 java -Xms1024g -Xmx1024g -XX:+UseG1GC  -jar ../target/PS5-1.0-SNAPSHOT-jar-with-dependencies.jar -type 0 --A $A >> Uniform_TestA.txt
# done                         

# echo "Varying B"
# for B in 100 200 500 
# do
   # numactl --cpunodebind=0 --membind=0 java -Xms1024g -Xmx1024g -XX:+UseG1GC  -jar ../target/PS5-1.0-SNAPSHOT-jar-with-dependencies.jar -type 0 --B $B >> Uniform_TestB.txt
# done 

echo "Varying M"
for M in 500 800 1000
do
   numactl --cpunodebind=0 --membind=0 java -Xms1024g -Xmx1024g -XX:+UseG1GC  -jar ../target/PS5-1.0-SNAPSHOT-jar-with-dependencies.jar -type 0 --M $M >> Uniform_TestM.txt
done 


# echo "Expoential distribution experiments"

# echo "Varying A"
# for A in 100 200 500 
# do
   # numactl --cpunodebind=0 --membind=0 java -Xms1024g -Xmx1024g -XX:+UseG1GC  -jar ../target/PS5-1.0-SNAPSHOT-jar-with-dependencies.jar -type 1 --A $A >> Expoential_TestA.txt
# done                         

# echo "Varying B"
# for B in 100 200 500 
# do
   # numactl --cpunodebind=0 --membind=0 java -Xms1024g -Xmx1024g -XX:+UseG1GC  -jar ../target/PS5-1.0-SNAPSHOT-jar-with-dependencies.jar -type 1 --B $B >> Expoential_TestB.txt
# done 

# echo "Varying M"
# for M in 500 800 1000 
# do
   # numactl --cpunodebind=0 --membind=0 java -Xms1024g -Xmx1024g -XX:+UseG1GC  -jar ../target/PS5-1.0-SNAPSHOT-jar-with-dependencies.jar -type 1 --M $M >> Expoential_TestM.txt
# done 

